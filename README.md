# Emailer

Android application to send email from contact.

## Getting Started

Clone this repository in your android studio and open it as an android application.

### Prerequisites

Android Studio and everything necessary for running android application like Java, Android SDK


## Built With

* [Android Studio](https://developer.android.com/studio) - The IDE for Android Applicaiton Development


## Results

* [Wiki](https://bitbucket.org/tekrajchhetri/emailer/wiki/Home) - Visit the links to see the snapshots of app.


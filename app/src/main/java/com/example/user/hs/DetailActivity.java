package com.example.user.hs;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DetailActivity extends AppCompatActivity {


    TextView name;
    TextView email;
    TextView contact;
    Button sendMailbutton;
    EditText emailTitle;
    EditText emailBody;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layoug);
        name = findViewById(R.id.name_contact);
        email = findViewById(R.id.email_contact);
        contact = findViewById(R.id.phone_contact);
        sendMailbutton = findViewById(R.id.button);
        emailTitle = findViewById(R.id.email_title);
        emailBody = findViewById(R.id.email_body);
        findDetailsByNameandSet();

        sendMailbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = email.getText().toString();
                String regex = "^(.+)@(.+)$";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(id);

                if (matcher.matches()){
                    sendMail();
                }else{
                    Toast toast = Toast.makeText(getApplicationContext(),"Mail can't be send without ID",Toast.LENGTH_SHORT);
                    toast.show();
                }


            }
        });
    }

    private void sendMail() {

        String recipientList = email.getText().toString();
        String[] recipients = recipientList.split(",");

        String subject = emailTitle.getText().toString();
        String message = emailBody.getText().toString();

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);

        intent.setType("message/rfc822");
        startActivityForResult(Intent.createChooser(intent, "Choose an email client"),101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 101){
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void findDetailsByNameandSet() {
        Intent intent = getIntent();
        String selectionArgument = intent.getStringExtra("contact_name");
        ContentResolver contentResolver = getContentResolver();
        StringBuilder stringBuilder = new StringBuilder();
        Cursor cursor = contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + "= ?",
                new String[]{selectionArgument},
                null);

        String emailID = "";
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                String contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                String setname = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String setphone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                if (setname != null && setname.trim().length() > 0){
                    name.setText(setname);
                }else {
                    name.setText("No name in contact");
                }

                if (setname != null && setname.trim().length() > 0){
                    contact.setText(setphone);
                }else {
                    contact.setText("XXXXXXXXXX");
                }



                Cursor emailsCursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactID, null, null);


                if (emailsCursor.moveToNext()) {
                    emailID = emailsCursor.getString(emailsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));


                }

            }
            if (emailID != null && emailID.trim().length() > 0) {
                email.setText(emailID);
            }else{
                email.setText("No Email in Contact");
            }

            cursor.close();


        }
    }


}



package com.example.user.hs;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

public class MainActivity extends AppCompatActivity {

    final String [] requestedPermissions = new String[]{
            Manifest.permission.READ_CONTACTS,

    };

    final int PERMISSIONCODE = 14;
    ListView listView;
    SearchView searchView;
    String mSearchQuery = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActivityCompat.requestPermissions(this,requestedPermissions,PERMISSIONCODE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name =  ((TextView)view.findViewById(R.id.name_contact)).getText().toString();
                startSecondActivity(name);
            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("permission","Permission granted");
        if (mSearchQuery != null && mSearchQuery.trim().length() > 0){


            showListItem(mSearchQuery);
        }else{
            showListItem(null);
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        SearchView  search = findViewById(R.id.app_bar_search);
        outState.putString("searchKey",search.getQuery().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        String data = savedInstanceState.getString("searchKey");
        mSearchQuery = data;
    }

    private void showListItem(String condition ){


        String[] SELECTED_COLUMNS = {
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone._ID,

        };

        int[] toViews_list_linear_layout = {R.id.name_contact};
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Cursor cursor;
        if (condition == null){
             cursor = getContentResolver().query(uri,
                    SELECTED_COLUMNS,
                    null,
                    null,
                     ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME +" ASC"
            );

        }else {
            cursor = getContentResolver().query(uri,
                    SELECTED_COLUMNS,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+ " LIKE '%" + condition + "%'",
                    null,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME +" ASC"
            );

       }

         SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(
                this, // context
                R.layout.contactlistview_linear_layout, // layout that defines the views for this list item.
                cursor, // The database cursor
                SELECTED_COLUMNS, // "from" - column names of the data to bind to the UI
                toViews_list_linear_layout, // TextViews that should display column in the "from" param
                0 // Flags used to determine the behavior of the adapter
        );


         listView.setAdapter(cursorAdapter);



    }

    private void startSecondActivity(String name){
        Intent intent = new Intent(this,DetailActivity.class);
        intent.putExtra("contact_name",name);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.searchmenu,menu);
        MenuItem menuItem = menu.findItem(R.id.app_bar_search);
       searchView = (SearchView) menuItem.getActionView();
        if (mSearchQuery != null && mSearchQuery.trim().length() > 0){
            searchView.setQuery(mSearchQuery, false);
            searchView.setIconified(false);
            searchView.clearFocus();
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() == 0){
                    showListItem(null);
                }else {
                    showListItem(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0){
                    showListItem(null);
                }else {
                    showListItem(newText);
                }

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }





}
